import { Component, Inject, Injector } from '@angular/core';
import { Location, LocationStrategy } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

import {DepositService} from "./services/deposit.service";
import {KycService} from "./services/kyc.service";
import {TimeService} from "./services/time.service";

import * as constants from './constants/authipayAPI';
import {AuthipayService} from "./services/authipay/new/authipay.service";
import {FixtureService} from "./services/fixture.service";

import * as Parse from 'parse';
import * as constantsFan from './constants/constants_fanager';
import {Fixture} from "./models/fixture";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'New docker-ms project works';

  timeService:TimeService;
  depositService:DepositService;
  kycService:KycService;
  authipayService:AuthipayService;
  fixtureService:FixtureService;

  responce:any;

  constructor(kycService:KycService, authipayService:AuthipayService, public inj:Injector ) {
    Parse.initialize(constantsFan.AppId, constantsFan.JsKey, constantsFan.AppKey);
    (<any>Parse).serverURL = constantsFan.AppUrl;
    console.log('Parse Server initialised with ' +constantsFan.AppId);

    this.kycService = kycService;
    this.authipayService = authipayService;
    this.fixtureService = inj.get(FixtureService);
    // this.timeService = timeService;
  }

  /** ngOnInit()
   *  - Initiases Parse Server credentials
   */
  ngOnInit() {
    this.getFixture();
  }

  fixture:Fixture;
  getFixture() {
    this.fixtureService.getTestFixture();
    setInterval( () =>  this.fixture = this.fixtureService.fixture, 1000);
  }

  requestKYCMinData() {
    this.kycService.getKYCScoreForUser(this.params);
  }

  requestKYCDocData() {
    this.kycService.getKYCDocScoreForUser(this.paramsDoc);
  }

  requestKYCReferData() {
    this.kycService.getKYCScoreForUser(this.paramsRefer);
  }

  sendSaleData() {
    this.authipayService.sendSale(this.setUpXMLPostBody());
  }

  handleFiles(files) {
    console.log('Files length ' +files.length);
  }

  setUpXMLPostBody() {
    return constants.sale;
  }

  params = {
    IDUDetails: {
      Login: {
        username: 1842965,
        password: 'Florida33'
      },
      Services: {
        address: 1,
        //dob: 1,
        deathscreen: 1,
        sanction: 1,
        insolvency: 1,
        // crediva: 1,
        ccj: 1,
        // passport: 1
      },
      Person: {
        address1: '20/2 bruntsfield avenue',
        address2: 'Edinburgh',
        address3: '',
        address4: '',
        address5: '',
        address6: '',
        postcode: 'EH10 4EW',
        // dob: '1964-05-28',
        forename: 'Duncan',
        surname: 'McMillan',
        // gender: 'M',
      }
    }
  };

  paramsRefer = {
    IDUDetails: {
      Login: {
        username: 1842965,
        password: 'Florida33'
      },
      Services: {
        address: 1,
        dob: 1,
        deathscreen: 1,
        sanction: 1,
        insolvency: 1,
        // crediva: 1,
        ccj: 1,
        // passport: 1
      },
      Person: {
        address1: '20/2 bruntsfield avenue',
        address2: 'Edinburgh',
        address3: '',
        address4: '',
        address5: '',
        address6: '',
        postcode: 'EH10 4EW',
        // dob: '1964-05-28',
        forename: 'Joe',
        surname: 'Bloggs',
        // gender: 'M',
      }
    }
  };

  paramsDoc = {
    IDUDetails: {
      Login: {
        username: 1842965,
        password: 'Florida33'
      },
      Services: {
        address: 1,
        dob: 1,
        deathscreen: 1,
        sanction: 1,
        insolvency: 1,
        // crediva: 1,
        ccj: 1,
        passport: 1
      },
      Person: {
        address1: '20/2 bruntsfield avenue',
        address2: 'Edinburgh',
        address3: '',
        address4: '',
        address5: '',
        address6: '',
        postcode: 'EH10 4EW',
        dob: '1964-05-28',  // Needed fro passport checks
        forename: 'Duncan',
        surname: 'McMillan',
        gender: 'M',  // Needed fro passport checks
        passport1: '8',
        passport2: '0',
        passport3: '1',
        passport4: '5',
        passport5: '3',
        passport6: '4',
        passport7: '5',
        passport8: '0'
      }
    }
  };
}
