//import * as constants from '../constants/constants';
import {Injectable, OnInit, Inject} from '@angular/core';
import { Fixture } from '../models/fixture';
import * as Parse from 'parse';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import {Contest} from "../models/contest";
//import {TimeService} from "./time.service";

@Injectable()
export class FixtureService {

  constructor() {
    this.getLiveFixtureUpdate();
    this.getFixtureStatusForContest();
  }

  /** Live Query for the following:
   *  - Update live scoring on Fixture component
   */
  fixtureObject = Parse.Object.extend("Fixture");

  public fixturesLiveUpdateSource = new Subject<Fixture>();
  public fixturesLiveUpdateAnnounce = this.fixturesLiveUpdateSource.asObservable();
  getLiveFixtureUpdate() {
    let queryFixtureLive:any = new Parse.Query(this.fixtureObject);
    queryFixtureLive.equalTo('status', 'Live');
    let subscriptionFixtureLive = queryFixtureLive.subscribe();

    subscriptionFixtureLive.on('open', (object) => {
      console.log('Fixture Socket Opened');
    });

    subscriptionFixtureLive.on('create', (object) => {
      console.log('Fixture Created');
    });

    subscriptionFixtureLive.on('enter', (object) => {
      console.log('Fixture Enter');
    });

    subscriptionFixtureLive.on('error', (error) => {
      console.log('Fixture error:' + error.message);
    });

    subscriptionFixtureLive.on('update', (object) => {
      console.log('Live Fixture Update');
      console.log('fixtureID: ' + object.get('fixtureID'));
      this.fixturesLiveUpdateSource.next(object);
    });
  }

  /** Live Query for the following:
   *  - Update live fixture status on Fixture component, really just for when
   *    fixture moves from Live to Closed when its associated Contest is still open.
   */
  public fixturesStatusUpdateSource = new Subject<Fixture>();
  public fixturesStatusUpdateAnnounce = this.fixturesStatusUpdateSource.asObservable();
  getFixtureStatusForContest() {

  }

  /** Test purposes only **/
  fixture:Fixture;
  getTestFixture() {
    var that = this;
    var query = new Parse.Query('Fixture');
    query.equalTo('fixtureID', 855549);    /** Change this for test **/
    query.find({
      success: function (results) {
        if (results) {
          results.forEach((fixture) => {
            //console.log('Fixture received with ID ' + fixture.id + " name " + fixture.get('teamNameHome'));
            let fix = new Fixture();
            fix.teamNameAwayAbbreviation = fixture.get('teamNameAwayAbbreviation');
            fix.fixtureID = fixture.get('fixtureID');
            fix.gameweek = fixture.get('gameweek');
            fix.teamIDAway = fixture.get('teamIDAway');
            fix.teamNameHome = fixture.get('teamNameHome');
            fix.status = fixture.get('status');
            fix.teamNameHomeAbbreviation = fixture.get('teamNameHomeAbbreviation');
            fix.teamNameAway = fixture.get('teamNameAway');
            fix.teamIDHome = fixture.get('teamIDHome');
            fix.competitionID = fixture.get('competitionID');
            fix.seasonID = fixture.get('seasonID');
            fix.teamScoreHome = fixture.get('teamScoreHome');
            fix.teamScoreAway = fixture.get('teamScoreAway');
            fix.date = that.formatDateTime(fixture.get('date'));
            fix.shortDate = that.formatShortDayTime(fixture.get('date'));

            that.fixture = fix;
          });
        }
      },
      error: function (error) {
        console.log('Error Getting Fixtures With ID ' + error);
      }
    });
  }

  /**
   * formatLongTime
   * @param time
   * @returns {string}
   */
  formatShortDayTime(time:string) {
    let date = moment(time).format('ddd H:mm');
    return date.toString();
  }

  /**
   * formatDateTime -
   * @param time
   */
  formatDateTime(time:string) {
    let date = moment(time).format('DD-MM-YY h:mm a');
    return date.toString();
  }

}
