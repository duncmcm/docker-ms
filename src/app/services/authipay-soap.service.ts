import { Injectable } from '@angular/core';
// import * as soap from 'soap';
import * as constants from '../constants/authipay';
import * as api from '../constants/authipayAPI';
import { Http, Response, BaseRequestOptions, RequestMethod, Request, Headers, Jsonp, URLSearchParams } from '@angular/http';

@Injectable()
export class AuthipaySoapService {

  xmlHttp:XMLHttpRequest = new XMLHttpRequest();

  constructor(public http: Http) {
    this.getCertFiles();

    this.envelopedRequest = this.envelopeBuilder(api.sale);
    this.setUpHandlers();
    // this.initSoapClient();
  }

  args;
  options;
  envelopedRequest;

  file;

  initSoapClient() {
    this.xmlHttp.open("POST", constants.url,);
    this.xmlHttp.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    this.xmlHttp.setRequestHeader("Authentication", "Basic " +this.setBasicAuth());

    this.xmlHttp.send(this.envelopedRequest);

    console.log("File " +this.certFile);
  }

  certFile;
  getCertFiles() {
    this.http.get('/src/app/keys/client/WS13205400604._.1.key').map((res:Response) => this.certFile = res.json() );
  }

  setUpHandlers() {
    this.xmlHttp.onreadystatechange = () => {
      console.log('XMLHttpRequest ready state: ' + this.xmlHttp.readyState);
    }
  }

  setBasicAuth() {
    let str = +"WS" +constants.storename +"._." +constants.userId +":" +constants.password;
    return btoa(str);
  }

  private envelopeBuilder(requestBody:string):string {
    return "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
      "<SOAP-ENV:Header>" +
      "</SOAP-ENV:Header>" +
      "<SOAP-ENV:Body>" +
      requestBody +
      "</SOAP-ENV:Body>" +
      "</SOAP-ENV:Envelope>";
  }

}
