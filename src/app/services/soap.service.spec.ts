import { TestBed, inject } from '@angular/core/testing';

import { SoapService } from './soap.service';

describe('SoapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SoapService]
    });
  });

  it('should ...', inject([SoapService], (service: SoapService) => {
    expect(service).toBeTruthy();
  }));
});
