import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestMethod, Request, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class TimeService {

  //private timeUrl = 'http://192.168.99.100/app/api/parsetime';  // URL to web API
  private timeUrl = 'http://localhost:1338/api/parsetime';  // URL to web API

  period:number = 2000; /** 1 second **/
  interval;
  req;

  constructor(public http: Http) {
    this.interval = setInterval( () => {
      this.getTime();
    }, this.period);
  }

  //body;
  //setUpPostBody() {
  //  let urlSearchParams = new URLSearchParams();
  //  urlSearchParams.append('message', 'Hello Duncan');
  //  return urlSearchParams.toString();
  //}

  time:any;
  getTime() {
    console.log('Asking for time ');
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    //this.body = this.setUpPostBody();

    this.http.post(this.timeUrl, this.params, {
          headers: headers
        })
        .map(res => res.text())
        .subscribe(
            data => this.time = data,
            err => this.logError(err),
            () => console.log('Get Time Complete ' +this.time)
        );
  }

  logError(err) {
    console.error('There was an error: ' + err);
  }

  params = {
  IDUDetails: {
    Login: {
      username: 1842965,
      password: 'Florida33'
    },
    Services: {
      address: 1,
      dob: 1,
      deathscreen: 1,
      sanction: 1,
      insolvency: 1,
      // crediva: 1,
      ccj: 1,
      // passport: 1
    },
    Person: {
      address1 :'20/2 bruntsfield avenue',
      address2 :'Edinburgh',
      address3 :'',
      address4 :'',
      address5 :'',
      address6 :'',
      postcode: 'EH10 4EW',
      dob: '1964-05-28',
      forename: 'Duncan',
      surname: 'McMillan',
      gender: 'M',
      passport1: '8',
      passport2: '0',
      passport3: '1',
      passport4: '5',
      passport5: '3',
      passport6: '4',
      passport7: '5',
      passport8: '0'
    }
  }
};

}
