import { TestBed, inject } from '@angular/core/testing';

import { KycStrongSoapService } from './kyc-strong-soap.service';

describe('KycStrongSoapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KycStrongSoapService]
    });
  });

  it('should ...', inject([KycStrongSoapService], (service: KycStrongSoapService) => {
    expect(service).toBeTruthy();
  }));
});
