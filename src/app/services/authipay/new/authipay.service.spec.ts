import { TestBed, inject } from '@angular/core/testing';

import { AuthipayService } from './authipay.service';

describe('AuthipayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthipayService]
    });
  });

  it('should ...', inject([AuthipayService], (service: AuthipayService) => {
    expect(service).toBeTruthy();
  }));
});
