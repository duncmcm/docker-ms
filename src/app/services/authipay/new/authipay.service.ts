import { Injectable } from '@angular/core';
import * as constants from '../../../constants/authipayAPI';
//import * as fs from 'fs';
//import * as https from 'https';
import * as crypto from 'crypto-js';
import { Http, Response, BaseRequestOptions, RequestMethod, Request, Headers, Jsonp, URLSearchParams } from '@angular/http';

@Injectable()
export class AuthipayService {

  url = '/authipay/saletest';

  storeID = '13205400604';
  userID = 'WS13205400604._.1';
  password = 'y2(:R4rdpq';

  options = {
    hostname: 'localhost',
    port: 8084,
    path: this.url,
    method: 'POST',
    //ca: fs.readFileSync('ca-crt.pem')
  };

  constructor(public http:Http) { }

  authipayData:any;
  sendSale(params:any) {
    let headers = new Headers();
    headers.append("Content-Type", "text/xml; charset=utf-8");
    headers.append("Authorization", "Basic " +this.makeHash());

    this.http.post(this.url, params, {
        headers: headers
      })
      .map(res => res.text())
      .subscribe(
        data => this.authipayData = data,
        err => this.logError(err),
        () => console.log('Get Auhtipay Complete ' +this.parseData(this.authipayData) )
      );
  }

  parseData(data:any) {
    let parsedData:any = {};

    let json = JSON.parse(data);
    parsedData.kycScore = json.Results.Summary.Smartscore;
    parsedData.kycStatus = json.Results.Summary.ResultText;

    console.log("In ParseData : score " +parsedData.kycScore.$value +" status : " +parsedData.kycStatus.$value);
  }

  logError(err) {
    console.error('There was an error: ' + err);
  }

  makeHash() {
    let stringToHash = "WS" +this.storeID  +"._." +this.userID +":" +this.password;
    console.log('String ' +stringToHash);

    let result = this.ascii_to_hexa(stringToHash); //crypto.SHA256(this.ascii_to_hexa(stringToHash));
    console.log('H ' +result);

    return result;
  }

  ascii_to_hexa(str) {
    var arr1 = [];
    for (var n = 0, l = str.length; n < l; n ++)
    {
      var hex = Number(str.charCodeAt(n)).toString(16);
      arr1.push(hex);
    }
    return 'V1MxMzIwNTQwMDYwNC5fLldTMTMyMDU0MDA2MDQuXy4xOnkyKDpSNHJkcHE=';
    // return arr1.join('');
  }

  setUpXMLPostBody() {
    return constants.sale;
  }
}
