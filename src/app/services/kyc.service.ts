import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestMethod, Request, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class KycService {

  private kycMinUrl = '/kyc/atmmincheck'; //'http://localhost:2338/kyc/atmmincheck';  // URL to node KYC API
  private kycDocUrl = 'http://localhost:2338/kyc/atmdoccheck';  // URL to node KYC API

  constructor(public http:Http) {

  }

  kycData:any;
  getKYCScoreForUser(params:any) {
    console.log('Asking for KYC Data ');
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(this.kycMinUrl, params, {
      headers: headers
    })
      .map(res => res.text())
      .subscribe(
        data => this.kycData = data,
        err => this.logError(err),
        () => console.log('Get KYC Complete ' +this.parseData(this.kycData) )
      );
  }

  getKYCDocScoreForUser(params:any) {
    console.log('Asking for KYC Data ');
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(this.kycDocUrl, params, {
      headers: headers
    })
      .map(res => res.text())
      .subscribe(
        data => this.kycData = data,
        err => this.logError(err),
        () => console.log('Get KYC Complete' +this.parseData(this.kycData))
      );
  }

  parseData(data:any) {
    let parsedData:any = {};

    let json = JSON.parse(data);
    parsedData.kycScore = json.Results.Summary.Smartscore;
    parsedData.kycStatus = json.Results.Summary.ResultText;

    console.log("In ParseData : score " +parsedData.kycScore.$value +" status : " +parsedData.kycStatus.$value);
  }

  logError(err) {
    console.error('There was an error: ' + err);
  }

}
