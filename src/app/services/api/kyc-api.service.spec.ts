import { TestBed, inject } from '@angular/core/testing';

import { KycApiService } from './kyc-api.service';

describe('KycApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KycApiService]
    });
  });

  it('should ...', inject([KycApiService], (service: KycApiService) => {
    expect(service).toBeTruthy();
  }));
});
