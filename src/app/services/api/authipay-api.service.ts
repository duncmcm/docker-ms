import { Injectable } from '@angular/core';
import { Http, Response, BaseRequestOptions, RequestMethod, Request, Headers, Jsonp, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import * as crypto from 'crypto-js';
import * as constants from '../../constants/authipayAPI';

@Injectable()
export class AuthipayApiService {

  private depositUrl = 'http://localhost:1339/api/deposit';  // URL to web API

  private authConnectUrl = 'https://test.ipg-online.com/ipgapi/services';
  private storeID = '13205400604';
  private userID = 'WS13205400604._.1';
  private password = 'y2(:R4rdpq';

  depositValue:string = '120.00';
  currency = '826';

  constructor(public http: Http) {
    //this.sendTestDeposit();
    this.sendDeposit();
  }

  public depositSource = new Subject<string>();
  public depositAnnounce = this.depositSource.asObservable();

  responce:any;
  body;

  sendDeposit() {
    let headers = new Headers();
    headers.append('Content-Type', 'text/xml');
    headers.append('Authorization', 'Basic ' +this.makeHash());

    this.body = this.setUpXMLPostBody();

    this.http.post(this.authConnectUrl, this.body , {
      headers: headers
    })
      .map(res => {
        this.responce = res.text();
        this.depositSource.next(this.responce);
      })
      .subscribe(
        data => this.saveToken(data),
        err => this.logError(err),
        () => console.log('Deposit Complete')
      );
  }

  setUpPostBody() {
    this.time = this.formatDateTime(new Date().toISOString());
    let h:string = this.makeHash();
    let urlSearchParams = new URLSearchParams();

    urlSearchParams.append('txntype', 'sale');
    urlSearchParams.append('timezone', 'Europe/London');
    urlSearchParams.append('txndatetime', this.time);
    urlSearchParams.append('currency', this.currency);
    urlSearchParams.append('hash_algorithm', 'SHA256');
    urlSearchParams.append('storename', '13205400604');
    urlSearchParams.append('mode', 'payonly');
    urlSearchParams.append('chargetotal', this.depositValue);
    urlSearchParams.append('responseSuccessURL', 'https://dev.fanager.com/');
    urlSearchParams.append('responseFailURL', 'https://dev.fanager.com/');
    urlSearchParams.append('customerid', 'abcd');
    urlSearchParams.append('hash', h);

    return urlSearchParams.toString();
  }

  /**
   * formatDateTime -
   * @param time
   */
  time:string;
  formatDateTime(time:string) {
    let date = moment(time).format('YYYY:MM:DD-HH:mm:ss');
    console.log("Timeset to " +date);
    return date.toString();
  }

  makeHash() {
    let stringToHash = "WS" +this.storeID  +"._." +this.userID +":" +this.password;
    console.log('String ' +stringToHash);

    let result = crypto.SHA256(this.ascii_to_hexa(stringToHash));
    console.log('H ' +result);

    return result;
  }

  ascii_to_hexa(str) {
    var arr1 = [];
    for (var n = 0, l = str.length; n < l; n ++)
    {
      var hex = Number(str.charCodeAt(n)).toString(16);
      arr1.push(hex);
    }
    return arr1.join('');
  }

  saveToken(value:any) {
    // this.depositValue = parseInt(value);
    // console.log(this.responce);
    // document.getElementById('body').setAttribute('innerHTML', value);
  }

  logError(err) {
    console.error('There was an error: ' + err);
  }


  // sendTestDeposit() {
  //   let headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //
  //   this.http.post(this.depositUrl, {value: 123, userId: 'abcd'}, {
  //     headers: headers
  //   })
  //     .map(res => res.json())
  //     .subscribe(
  //       data => this.saveToken(data.deposit),
  //       err => this.logError(err),
  //       () => console.log('Deposit Complete : ' +this.depositValue)
  //     );
  // }

  setUpXMLPostBody() {
    return constants.sale;
  }
}
