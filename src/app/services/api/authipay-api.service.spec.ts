import { TestBed, inject } from '@angular/core/testing';

import { AuthipayApiService } from './authipay-api.service';

describe('AuthipayApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthipayApiService]
    });
  });

  it('should ...', inject([AuthipayApiService], (service: AuthipayApiService) => {
    expect(service).toBeTruthy();
  }));
});
