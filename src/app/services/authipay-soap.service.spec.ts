import { TestBed, inject } from '@angular/core/testing';

import { AuthipaySoapService } from './authipay-soap.service';

describe('AuthipaySoapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthipaySoapService]
    });
  });

  it('should ...', inject([AuthipaySoapService], (service: AuthipaySoapService) => {
    expect(service).toBeTruthy();
  }));
});
