import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ChangeDetectorRef } from '@angular/core';
import {FixtureService} from "../../services/fixture.service";

@Component({
  selector: 'app-fixture',
  templateUrl: './fixture.component.html',
  styleUrls: ['./fixture.component.css']
})
export class FixtureComponent implements OnInit {

  @Input() fixture;
  showScore:boolean = true;

  fixtureService:FixtureService;
  constructor(fixtureService:FixtureService) {
    this.fixtureService = fixtureService;
  }

  /** Check how we:
   *  - Change fixture object scores and status through :
   *      - ChangeDetectorRef, ChangeDetectionStrategy, async pipe
   */
  ngOnInit() {
    this.fixtureService.fixturesLiveUpdateAnnounce.subscribe(
        fixture => {
          if (this.fixture.fixtureID == fixture.fixtureID) {
            console.log("Fixture update in Fixture component " +fixture.fixtureID);
            this.fixture = fixture;
            //this.ref.detectChanges();
          }
        });
  }

  ngDestroy() {

  }
}
