/**
 * Created by duncmcm on 14/04/2017.
 */
export class KycPerson {

  makeObject() {
    // let Reference = this.Reference;
    // let ID = this.ID;
    // let IKey = this.IKey;
    // let Scorecard = this.Scorecard;
    // let equifaxUsername = this.equifaxUsername;

    return {
      // Reference,
      // ID,
      // IKey,
      // Scorecard,
      // equifaxUsername
    }
  }

  public forename                       = 'Colin';
  public middle                         = 'Graham';
  public surname                        = 'Gowan';
  public gender                         = 'M';
  public dob                            = '1962-01-01';

  public address1                       = '85 Julius Road';
  public address2                       = 'Bristol';
  public address3                       = '';
  public address4                       = '';
  public address5                       = '';
  public address6                       = '';
  public postcode                       = 'BS7 8EU';

  public passport1                      = '';
  public passport2                      = '';
  public passport3                      = '';
  public passport4                      = '';
  public passport5                      = '';
  public passport6                      = '';
  public passport7                      = '';
  public passport8                      = '';

  public travelvisa1                    = '';
  public travelvisa2                    = '';
  public travelvisa3                    = '';
  public travelvisa4                    = '';
  public travelvisa5                    = '';
  public travelvisa6                    = '';
  public travelvisa7                    = '';
  public travelvisa8                    = '';
  public travelvisa9                    = '';

  public idcard1                        = '';
  public idcard2                        = '';
  public idcard3                        = '';
  public idcard4                        = '';
  public idcard5                        = '';
  public idcard6                        = '';
  public idcard7                        = '';
  public idcard8                        = '';
  public idcard9                        = '';
  public idcard10                       = '';

// Driving Licence
  public drivinglicence1                = '';
  public drivinglicence2                = '';
  public drivinglicence3                = '';

// Card Number
  public cardnumber                     = '';
  public cardtype                       = '';

// NI
  public ni                             = '';

// NHS
  public nhs                            = '';

// Birth Details
  public bforename                      = '';
  public bmiddle                        = '';
  public bsurname                       = '';
  public maiden                         = '';
  public bdistrict                      = '';
  public bcertificate                   = '';

// Electricity Bill
  public mpannumber1                    = '';
  public mpannumber2                    = '';
  public mpannumber3                    = '';
  public mpannumber4                    = '';

// Bank Account
  public sortcode                       = '';
  public accountnumber                  = '';

// Marriage Details
  public msubjectforename               = '';
  public msubjectsurname                = '';
  public mpartnerforename               = '';
  public mpartnersurname                = '';
  public mdate                          = '';
  public mdistrict                      = '';
  public mcertificate                   = '';

// Poll Number Details
  public pollnumber                     = '';

// Email Details
  public email                          = '';
  public email2                         = '';

// Document Authentication Details
  public docfront                       = '';
  public docback                        = '';
  public docsize                        = '';

// One Time Password Details
  public landline1                      = '';
  public landline2                      = '';
  public mobile1                        = '';
  public mobile2                        = '';
  public otplandline1                   = '';
  public otplandline2                   = '';
  public otpmobile1                     = '';
  public otpmobile2                     = '';

}
