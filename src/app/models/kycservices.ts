/**
 * Created by duncmcm on 14/04/2017.
 */
export class KycServices {

// Enable minimum services required for AML
  public address                      = 1;
  public deathscreen                  = 1;
  public dob                          = 1;
  public sanction                     = 1;
  public insolvency                   = 1;
  public crediva                      = 1;
  public ccj                          = 1;

  makeObject() {
    let address = this.address;
    let deathscreen = this.deathscreen;
    let dob = this.dob;
    let sanction = this.sanction;
    let insolvency = this.insolvency;
    let crediva = this.crediva;
    let ccj = this.ccj;

    return {
      address,
      deathscreen,
      dob,
      sanction,
      insolvency,
      crediva,
      ccj
    }
  }

}
