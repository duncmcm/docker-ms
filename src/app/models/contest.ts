/**
 * Created by duncmcm on 07/12/2016.
 */
import * as Parse from 'parse';
//import {UserTeam} from "./userteam";

export class Contest extends Parse.Object {

    public prizes:any;
    public prizePoolPoundsPence:string;
    public fixtures:any;
    public name:string;
    public entryFee:number;
    public entryFeePoundsPence:string;
    public prizeType:string;
    public contestType:string;
    public startDate:string;
    public startDateLong:string;
    public startDateDayFormat:string;
    public startDateTime:string;
    public startDayDateTime:string;
    public isoDate:Date;
    public entries:any;
    public status:string;
    public maxEntries:number;
    public maxEntriesPerUser:number;
    public presentEntriesForLoggedInUser:number = 0;
    public progressBarWidth:string;

    public id:any;
    public contestID:number;

    /** UI Requirement only
     *  - This closingInterval monitors when a Contest has CLOSED status in Parse server
     *    but is within 1 Hour after this CLOSED status being set.
     *  - This is used so that the user can still view this closed status contest to look
     *    through results
     *  - Set to arbitary figure of 120 minutes as the mycontest service would set it to 60 minute
     *    and count it down.
     */
    closingInterval:number = 120;
    isFull:boolean;

    /** UI Requirement only
     *  - used in the open contest table to define teams specific to a contest for each team row within the table
     */
    //teams:UserTeam[] = [];

    constructor() {
        super('Contest');
    }
}
