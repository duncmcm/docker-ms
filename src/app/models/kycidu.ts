/**
 * Created by duncmcm on 14/04/2017.
 */
export class KycIDU {

  constructor() {

  }

  public Reference                         = 'your-reference';
  public ID                                = '';
  public IKey                              = '';
  public Scorecard                         = 'IDU Default';
  public equifaxUsername                   = '';

  makeObject() {
    let Reference = this.Reference;
    let ID = this.ID;
    let IKey = this.IKey;
    let Scorecard = this.Scorecard;
    let equifaxUsername = this.equifaxUsername;

    return {
      Reference,
      ID,
      IKey,
      Scorecard,
      equifaxUsername
    }
  }
}
