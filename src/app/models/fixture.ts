/**
 * Created by duncmcm on 12/12/2016.
 */
export class Fixture {

    public teamNameAwayAbbreviation:string;
    public fixtureID:number;
    public gameweek:number;
    public teamIDAway:number;
    public teamNameHome:string;
    public status:string;
    public teamNameHomeAbbreviation:string;
    public teamNameAway:string;
    public teamIDHome:number;
    public competitionID:number;
    public seasonID:number;
    public teamScoreAway:number;
    public teamScoreHome:number;
    public id:any;
    public date:any;
    public shortDate:any;

    constructor() {

    }
}