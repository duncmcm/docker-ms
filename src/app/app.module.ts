import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { Location, LocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import {TimeService} from "./services/time.service";
import {DepositService} from "./services/deposit.service";
import {KycService} from "./services/kyc.service";
import {SoapService} from "./services/soap.service";
import {AuthipaySoapService} from "./services/authipay-soap.service";
import {AuthipayApiService} from "./services/api/authipay-api.service";
import {KycApiService} from "./services/api/kyc-api.service";
import {KycStrongSoapService} from "./services/kyc-strong-soap.service";
import {AuthipayService} from "./services/authipay/new/authipay.service";
import { FixtureComponent } from './components/fixture/fixture.component';
import {FixtureService} from "./services/fixture.service";

@NgModule({
  declarations: [
    AppComponent,
    FixtureComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule
  ],
  providers: [TimeService, FixtureService, DepositService, SoapService, KycService, AuthipaySoapService, AuthipayApiService, KycApiService, KycStrongSoapService, AuthipayService],
  bootstrap: [AppComponent, ]
})
export class AppModule { }
