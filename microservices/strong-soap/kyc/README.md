#### Database User table associated columns

- verified : false -> no KYC check has been completed for user
           : true  -> previous KYC check exists for user
 
- failedChecks : false -> Has failed the last check
               : true  => Has passed the last check
                      
- kycStatus:  null  -> no KYC checks have been complete on user
           : 'Fail' -> previous KYC check has failed for user
           : 'Pass' -> previous KYC check has passed for user
           : 'Refer'-> previous KYC check has refer for user
            
- kycScore : number -> from latest KYC check  
          
#### KYC Process

Triggers:
- 1st KYC Check when user selects withdraw Submit + user.verified = null          
- 2nd KYC Check when user at upload docs Submit + user.verified = false + kycStatus != pass
        
Redirection changes:
- Remove redirect (withdraw to get verified) if user.verified = null        
          
Redirect after checks:
- 1st check
          - fails : goes to Get Verified page
          - pass  : no where UI confirms with draw was OK
- 2nd check
          - pending manual check : message to let user know it is pending 
          - fail  : TBD
          - pass  : 3rd transaction check (for card details)
          
- Transaction check , no data for this yet , use dummy routes:
          - pass  : Progress withdraw
          - fail  : Go to deposit view for user to ipput card details
          
#### UI Requirements
          
- Card details on deposit view (Henry to progress)
- Admin only 2nd KYC check for passport / license 

          
          