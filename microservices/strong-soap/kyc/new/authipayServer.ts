/**
 * Created by duncmcm on 13/06/2017.
 */
import express = require('express');
import bodyParser = require('body-parser');
import https = require('https');
import util = require('util');
import fs = require('fs');

let app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//app.post('/authipay/sale', sendSale);

let options = { key: fs.readFileSync('keys/client/WS13205400604._.1.key'),
    cert: fs.readFileSync('keys/client/WS13205400604._.1.pem'),
    passphrase: 'ZM;iW}23yT' };

let server = https.createServer(options, app);
server.listen(8084); console.log('Server is running on port 8084');

