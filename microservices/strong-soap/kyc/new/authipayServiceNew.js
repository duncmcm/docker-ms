"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 12/06/2017.
 */
/**
 * Created by duncmcm on 05/06/2017.
 */
/** Add all the requires **/
var express = require("express");
var bodyParser = require("body-parser");
var fs = require("fs");
var WSSecurityCert = require('strong-soap').WSSecurityCert;
var soap = require('strong-soap').soap;
var wsdl = 'https://test.ipg-online.com/ipgapi/services/order.wsdl';
var url = 'https://test.ipg-online.com/ipgapi/services';
var XMLHandler = soap.XMLHandler;
var xmlHandler = new XMLHandler();
var storeID = '13205400604';
var userID = 'WS13205400604._.1';
var password = 'y2(:R4rdpq';
var certPassword = 'ZM;iW}23yT';
/** Create the express server **/
var app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
/** Set up the app.post API call and the associated callback **/
app.post('/authipay/saletest', callbackAuthipay);
/** Create the soap client **/
var privateKey = fs.readFileSync('keys/client/WS13205400604._.1.key');
var publicKey = fs.readFileSync('keys/client/WS13205400604._.1.pem');
var serverCert = fs.readFileSync('keys/server/geotrust.pem');
var username = "WS" + this.storeID + "._." + this.userID + ":" + this.password;
var wSSecurityCert = new WSSecurityCert(privateKey, publicKey, certPassword, 'utf8');
var options = {
    // rejectUnauthorized: true,
    // strictSSL: true,
    WSSecurityCert: wSSecurityCert,
    // key: privateKey,
    // cert: publicKey,
    // passphrase: certPassword,
    // ca: serverCert,
    wsdl_headers: {
        WSSecurityCert: wSSecurityCert,
        // rejectUnauthorized: true,
        // strictSSL: false,
        // //secureOptions: constants.SSL_OP_NO_TLSv1_2,
        // key: privateKey,
        // cert: publicKey,
        // passphrase: certPassword,
        // ca: serverCert,
        connection: 'keep-alive'
    },
    wsdl_options: {
        WSSecurityCert: wSSecurityCert
        // rejectUnauthorized: true,
        // strictSSL: false,
        // //secureOptions: constants.SSL_OP_NO_TLSv1_2,
        // key: privateKey,
        // cert: publicKey,
        // passphrase: certPassword,
        // ca: serverCert,
        // connection: 'keep-alive'
    }
};
var atmCheckMethod;
var client;
/** Call the soap method through the above callback **/
function callbackAuthipay(req, res) {
    console.log('HTTP Cors Request callbackATMMin', +req.body);
    soap.createClient(url, options, function (err, c) {
        client = c;
        atmCheckMethod = client['IPGApiOrder'];
    });
    atmCheckMethod(req.body, function (err, result, envelope, soapHeader) {
        //response envelope
        // console.log('Response Envelope: \n' + envelope);
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(result));
        console.log('HTTP Cors Server Sending data', JSON.stringify(result));
    });
}
/** Listen on the express server **/
var server = app.listen(2340, function () {
    var port = server.address().port;
    console.log('HTTP Cors Authipay Service listening on Port', port);
});
exports = module.exports = app;
//# sourceMappingURL=authipayServiceNew.js.map