/**
 * Created by duncmcm on 12/06/2017.
 */
/**
 * Created by duncmcm on 05/06/2017.
 */
/** Add all the requires **/
import express = require('express');
import bodyParser = require('body-parser');
import https = require('https');
import util = require('util');
import fs = require('fs');

let WSSecurityCert = require('strong-soap').WSSecurityCert;

let soap = require('strong-soap').soap;
let wsdl = 'https://test.ipg-online.com/ipgapi/services/order.wsdl';
let url = 'https://test.ipg-online.com/ipgapi/services';

let XMLHandler = soap.XMLHandler;
let xmlHandler = new XMLHandler();

let storeID = '13205400604';
let userID = 'WS13205400604._.1';
let password = 'y2(:R4rdpq';
let certPassword = 'ZM;iW}23yT';

/** Create the express server **/
let app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/** Set up the app.post API call and the associated callback **/
app.post('/authipay/saletest', callbackAuthipay);

/** Create the soap client **/
let privateKey = fs.readFileSync('keys/client/WS13205400604._.1.key');
let publicKey = fs.readFileSync('keys/client/WS13205400604._.1.pem');
let serverCert = fs.readFileSync('keys/server/geotrust.pem');

let username = "WS" +this.storeID +"._." +this.userID +":" +this.password;
let wSSecurityCert = new WSSecurityCert(privateKey, publicKey, certPassword, 'utf8');

let options = {
  // rejectUnauthorized: true,
  // strictSSL: true,
  WSSecurityCert : wSSecurityCert,
  // key: privateKey,
  // cert: publicKey,
  // passphrase: certPassword,
  // ca: serverCert,
  wsdl_headers: {
    WSSecurityCert : wSSecurityCert,
    // rejectUnauthorized: true,
    // strictSSL: false,
    // //secureOptions: constants.SSL_OP_NO_TLSv1_2,
    // key: privateKey,
    // cert: publicKey,
    // passphrase: certPassword,
    // ca: serverCert,
    connection: 'keep-alive'
  },
  wsdl_options: {
    WSSecurityCert : wSSecurityCert
    // rejectUnauthorized: true,
    // strictSSL: false,
    // //secureOptions: constants.SSL_OP_NO_TLSv1_2,
    // key: privateKey,
    // cert: publicKey,
    // passphrase: certPassword,
    // ca: serverCert,
    // connection: 'keep-alive'
  }
};

let atmCheckMethod:any;
let client:any;

/** Call the soap method through the above callback **/
function callbackAuthipay(req, res) {

  console.log('HTTP Cors Request callbackATMMin', +req.body);

  soap.createClient(url, options, function(err, c) {
    client = c;
    atmCheckMethod = client['IPGApiOrder'];
  });

  atmCheckMethod(req.body, function(err, result, envelope, soapHeader) {
    //response envelope
    // console.log('Response Envelope: \n' + envelope);

    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(result));
    console.log('HTTP Cors Server Sending data', JSON.stringify(result));
  });

}

/** Listen on the express server **/
let server = app.listen(2340, function () {
  let port = server.address().port;
  console.log('HTTP Cors Authipay Service listening on Port', port);
});

exports = module.exports = app;
