"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 13/06/2017.
 */
var express = require("express");
var bodyParser = require("body-parser");
var https = require("https");
var fs = require("fs");
var app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//app.post('/authipay/sale', sendSale);
var options = { key: fs.readFileSync('keys/client/WS13205400604._.1.key'),
    cert: fs.readFileSync('keys/client/WS13205400604._.1.pem'),
    passphrase: 'ZM;iW}23yT' };
var server = https.createServer(options, app);
server.listen(8084);
console.log('Server is running on port 8084');
//# sourceMappingURL=authipayServer.js.map