/**
 * Created by duncmcm on 05/06/2017.
 */
/** Add all the requires **/
import express = require('express');
import bodyParser = require('body-parser');
import https = require('https');
import util = require('util');

let soap = require('strong-soap').soap;
let url = 'https://iduws.tracesmart.co.uk/v4.7/index.php?wsdl';
let XMLHandler = soap.XMLHandler;
let xmlHandler = new XMLHandler();

/** Create the express server **/
let app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/** Set up the app.post API call and the associated callback **/
app.post('/kyc/atmmincheck', callbackATMMin);
app.post('/kyc/atmdoccheck', callbackDocMin);

/** Create the soap client **/
let options = {};
let atmCheckMethod:any;

let client:any;
// soap.createClient(url, options, function(err, c) {
//   client = c;
//   atmCheckMethod = client['IDUProcess'];
// });

/** Call the soap method through the above callback **/
function callbackATMMin(req, res) {

  console.log('HTTP Cors Request callbackATMMin', +req.body.IDUDetails.Login.username);
  soap.createClient(url, options, function(err, c) {
    client = c;
    atmCheckMethod = client['IDUProcess'];
  });

  atmCheckMethod(req.body, function(err, result, envelope, soapHeader) {
    //response envelope
    // console.log('Response Envelope: \n' + envelope);

    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(result));
    console.log('HTTP Cors Server Sending data', JSON.stringify(result));
  });

}

function callbackDocMin(req, res) {
  console.log('HTTP Cors Request callbackDocMin', +req.body.IDUDetails.Login.username);

  atmCheckMethod(req.body, function(err, result, envelope, soapHeader) {
    //response envelope
    // console.log('Response Envelope: \n' + envelope);

    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(result));
    console.log('HTTP Cors Server Sending data', JSON.stringify(result));
  });

}

/** Listen on the express server **/
let server = app.listen(2338, function () {
    let port = server.address().port;
    console.log('HTTP Cors KYC Service listening on Port', port);
});

exports = module.exports = app;
