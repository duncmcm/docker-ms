"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 05/06/2017.
 */
/** Add all the requires **/
var express = require("express");
var bodyParser = require("body-parser");
var soap = require('strong-soap').soap;
var url = 'https://iduws.tracesmart.co.uk/v4.7/index.php?wsdl';
var XMLHandler = soap.XMLHandler;
var xmlHandler = new XMLHandler();
/** Create the express server **/
var app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
/** Set up the app.post API call and the associated callback **/
app.post('/kyc/atmmincheck', callbackATMMin);
app.post('/kyc/atmdoccheck', callbackDocMin);
/** Create the soap client **/
var options = {};
var atmCheckMethod;
var client;
// soap.createClient(url, options, function(err, c) {
//   client = c;
//   atmCheckMethod = client['IDUProcess'];
// });
/** Call the soap method through the above callback **/
function callbackATMMin(req, res) {
    console.log('HTTP Cors Request callbackATMMin', +req.body.IDUDetails.Login.username);
    soap.createClient(url, options, function (err, c) {
        client = c;
        atmCheckMethod = client['IDUProcess'];
    });
    atmCheckMethod(req.body, function (err, result, envelope, soapHeader) {
        //response envelope
        // console.log('Response Envelope: \n' + envelope);
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(result));
        console.log('HTTP Cors Server Sending data', JSON.stringify(result));
    });
}
function callbackDocMin(req, res) {
    console.log('HTTP Cors Request callbackDocMin', +req.body.IDUDetails.Login.username);
    atmCheckMethod(req.body, function (err, result, envelope, soapHeader) {
        //response envelope
        // console.log('Response Envelope: \n' + envelope);
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(result));
        console.log('HTTP Cors Server Sending data', JSON.stringify(result));
    });
}
/** Listen on the express server **/
var server = app.listen(2338, function () {
    var port = server.address().port;
    console.log('HTTP Cors KYC Service listening on Port', port);
});
exports = module.exports = app;
//# sourceMappingURL=kycService.js.map