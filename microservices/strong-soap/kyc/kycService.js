/**
 * Created by duncmcm on 30/05/2017.
 */
"use strict";

var soap = require('strong-soap').soap;
var https = require('https');
// wsdl of the web service this client is going to invoke. For local wsdl you can use, url = './wsdls/stockquote.wsdl'
var url = 'https://iduws.tracesmart.co.uk/v4.7/index.php?wsdl';

var XMLHandler = soap.XMLHandler;
var xmlHandler = new XMLHandler();
var util = require('util');

var params = {
  IDUDetails: {
    Login: {
      username: 1842965,
      password: 'Florida33'
    },
    Services: {
      address: 1,
      dob: 1,
      deathscreen: 1,
      sanction: 1,
      insolvency: 1,
      // crediva: 1,
      ccj: 1,
      // passport: 1
    },
    Person: {
      address1 :'20/2 bruntsfield avenue',
      address2 :'Edinburgh',
      address3 :'',
      address4 :'',
      address5 :'',
      address6 :'',
      postcode: 'EH10 4EW',
      dob: '1964-05-28',
      forename: 'Duncan',
      surname: 'McMillan',
      gender: 'M',
      passport1: '8',
      passport2: '0',
      passport3: '1',
      passport4: '5',
      passport5: '3',
      passport6: '4',
      passport7: '5',
      passport8: '0'
    }
  }
};

var options = {};
soap.createClient(url, options, function(err, client) {

  var method = client['IDUProcess'];

  method(params, function(err, result, envelope, soapHeader) {
    //response envelope
    console.log('Response Envelope: \n' + envelope);
    //'result' is the response body
    // console.log('Result: \n' + JSON.stringify(result));

    // console.log('Header: \n' + JSON.stringify(soapHeader));

    // Convert 'result' JSON object to XML
    var node = xmlHandler.jsonToXml(null, null,
      XMLHandler.createSOAPEnvelopeDescriptor('soap'), result);
    var xml = node.end({pretty: true});
    console.log(xml);

    // Convert XML to JSON object
    var root = xmlHandler.xmlToJson(null, xml, null);
    console.log('%s', util.inspect(root, {depth: null}));

    // console.log("Parsed JSON: " +JSON.parse(util.inspect(root, {depth: null})))
  });
});

