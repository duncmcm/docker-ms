/**
 * Created by duncmcm on 08/06/2017.
 */
/**
 * Created by duncmcm on 05/06/2017.
 */
/** Add all the requires **/
import express = require('express');
import bodyParser = require('body-parser');
import https = require('https');
import util = require('util');

let soap = require('strong-soap').soap;
let url = 'https://test.ipg-online.com/ipgapi/services';
let wsdl = 'https://test.ipg-online.com/ipgapi/services/order.wsdl';
let XMLHandler = soap.XMLHandler;
let xmlHandler = new XMLHandler();

/** Create the express server **/
let app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/** Set up the app.post API call and the associated callback **/
app.post('/authipay/saletest', callbackAPSale);

/** Create the soap client **/
let options = {
    wsdl_headers: {
        rejectUnauthorized: false,
        strictSSL: false,
        //secureOptions: constants.SSL_OP_NO_TLSv1_2,
        key: 'keys/client/WS13205400604._.1.key',
        cert: 'keys/client/WS13205400604._.1.p12',
        connection: 'keep-alive'
    },
    wsdl_options: {
        connection: 'keep-alive'
    }
};
let apSaleMethod:any;

soap.createClient(url, {}, function(err, client) {

    var didEmitEvent = false;
    console.log("Error : " +err);
    client.on('soapError', function(err) {
        didEmitEvent = true;
        console.log("Soap Error: " +err.root.Envelope.Body.Fault);
    });
    client.MyOperation({}, function(err, result) {
        console.log("Event: " +didEmitEvent);
    });

    apSaleMethod = client['IPGApiOrder'];

});

/** Call the soap method through the above callback **/
function callbackAPSale(req, res) {

    console.log('HTTP Cors Request callbackAPSale', +req.body);

    apSaleMethod(req.body, function(err, result, envelope, soapHeader) {
        //response envelope
         console.log('Response Envelope: \n' + envelope);

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(result));
        console.log('HTTP Cors Server Sending data', JSON.stringify(result));
    });

}

/** Listen on the express server **/
let server = app.listen(2001, function () {
    let port = server.address().port;
    console.log('HTTP Cors Authipay Service listening on Port', port);
});

exports = module.exports = app;
