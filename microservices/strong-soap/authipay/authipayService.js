/**
 * Created by duncmcm on 31/05/2017.
 */
"use strict";

var soap = require('strong-soap').soap;
var https = require('https');
// wsdl of the web service this client is going to invoke. For local wsdl you can use, url = './wsdls/stockquote.wsdl'
var url = 'https://test.ipg-online.com/ipgapi/services';

var params = {
  // symbol: 'IBM'
};

var options = {};
soap.createClient(url, options, function(err, client) {
  var method = client['IPGApiOrderRequest'];

  client.setSecurity(new soap.BasicAuthSecurity('13205400604._.WS13205400604._.1', 'Florida33'));

  // client.setSecurity({
  //   addOptions: function (options) {
  //     options.username = '10007417';//'fantasysportsltd',
  //     options.password = 'Florida33',
  //       // options.cert = test.sslOptions.cert,
  //       // options.key = test.sslOptions.key,
  //       options.rejectUnauthorized = false;
  //     // options.secureOptions = constants.SSL_OP_NO_TLSv1_2;
  //     options.strictSSL = false;
  //     options.agent = new https.Agent(options);
  //   }
  // })

  method(requestArgs, function(err, result, envelope, soapHeader) {
    //response envelope
    console.log('Response Envelope: \n' + envelope);
    //'result' is the response body
    console.log('Result: \n' + JSON.stringify(result));

    console.log('Error: \n' + JSON.stringify(err));

  });
});
