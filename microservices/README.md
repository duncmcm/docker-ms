# Process

Create a 'microservices' directory at the root of your project and copy the package.json file.

>npm install
>tsd install express node socket.io
>tsc

If ts-node installed globally do:

>ts-node microservices/timeservice/timeservice
>ts-node truefxservice/truefxservice

# Config

The tsconfig.json file needs changing to exclude the whole 'microservices' directory created above, 
as shown below. This is jst required because the micro services lie in the same git project:

"exclude": [
    "node_modules",
    "dist",
    "typings",
    "microservices"
  ],
