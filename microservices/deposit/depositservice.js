"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 31/08/2016.
 */
var express = require("express");
var bodyParser = require("body-parser");
var app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.post('/api/deposit', callback);
function callback(req, res) {
    console.log('HTTP Cors Request', +req.data);
    if (req.method.toLowerCase() == 'post') {
        var deposit = req.body.value;
        var user = req.body.userId;
        console.log("Server deposit = " + deposit + ", Server user is " + user);
        res.end(JSON.stringify({ deposit: deposit, error: 0 }));
    }
}
;
var server = app.listen(1339, function () {
    var port = server.address().port;
    console.log('HTTP Cors Deposit Service listening on Port', port);
});
exports = module.exports = app;
//# sourceMappingURL=depositservice.js.map