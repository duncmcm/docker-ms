/**
 * Created by duncmcm on 31/08/2016.
 */
import express = require('express');
import bodyParser = require('body-parser');

let app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/api/deposit', callback);

function callback(req, res) {
    console.log('HTTP Cors Request', +req.data);

    if (req.method.toLowerCase() == 'post') {
        let deposit = req.body.value;
        let user = req.body.userId;
        console.log("Server deposit = "+deposit+", Server user is "+user);
        res.end(JSON.stringify({deposit: deposit, error: 0}));
    }
};

let server = app.listen(1339, function () {
    let port = server.address().port;
    console.log('HTTP Cors Deposit Service listening on Port', port);
});

exports = module.exports = app;