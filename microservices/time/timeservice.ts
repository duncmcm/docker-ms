/**
 * Created by duncmcm on 31/08/2016.
 */
import express = require('express');
import bodyParser = require('body-parser');

let app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/api/parsetime', callback);

function callback(req, res) {
    if (req.method.toLowerCase() == 'post') {
        let date = new Date();
        let data = {
            "hour" : date.getHours().toString(),
            "minute": date.getMinutes().toString(),
            "second": date.getSeconds().toString(),
            "password": req.body.IDUDetails.Login.password
        };
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(data));
        console.log('HTTP Cors Server Sending data', JSON.stringify(data));

    }
};

let server = app.listen(1338, function () {
    let port = server.address().port;
    console.log('HTTP Cors Time Service listening on Port', port);
});

exports = module.exports = app;
