
Remove any associated Docker Images
===================================

- docker images -a
- docker rmi imageID

Build
=====

- docker build -t time-service .
- docker images (check your image is built)

Deploy on a local container to test
===================================

To check running and non running containers and also remove a container using:

- docker ps (running containers)
- docker ps -a (available container built previously)
- docker rm containerID

Run the time-service as follows in a local container:

- docker run -it -p 1338:1338 time-service

