import { DockerMsPage } from './app.po';

describe('docker-ms App', () => {
  let page: DockerMsPage;

  beforeEach(() => {
    page = new DockerMsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
